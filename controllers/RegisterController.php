<?php

use models\UserModel;

require_once("../models/UserModel.php");

$model = UserModel::getInstance();
$status = $model->RegisterUser($_POST['login'], $_POST['password']);
$errs = array("login already taken");

if(!in_array($status, $errs)) {
    setcookie('login' ,$_POST['login'], time()+60*60*24*30, "/");
    setcookie('hash', $status,  time()+60*60*24*30, "/");
    header("location: /news/");
} else {
    header("location: /register/");
}
