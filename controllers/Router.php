<?php

namespace router;

use models\Model;

require_once('models/Model.php');

//Отвечает за роутинг страниц
class Router
{
    private static $instance = null;

    private function __construct()
    {
    }

    //Получение ссылки на объект роутера-одиночки
    static public function getInstance(): ?Router
    {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    //Функция принимает на вход путь и вызывает страницу, если находит нужный endpoint
    //Во всех остальных случаях вызывает страницу 404
    public function Route(string $path)
    {
        //страница "о нас"
        if(preg_match("/^about\/?$/", $path)) {
            require 'views/about.php';
            return;
        }

        //страница "правила"
        if(preg_match("/^rules\/?$/", $path)) {
            require 'views/rules.php';
            return;
        }

        //страница входа
        if(preg_match("/^login\/$/", $path) ) {
            require "views/UserAuth/login.php";
            return;
        }

        //страница регистрации
        if(preg_match("/^register\/$/", $path)) {
            require "views/UserAuth/register.php";
            return;
        }

        //endpoint выхода пользователя
        if(preg_match("/^logout\/$/", $path)) {
            setcookie('login' ,"", time()-60*60*24*30, "/");
            setcookie('hash', "",  time()-60*60*24*30, "/");

            require "views/UserAuth/login.php";
            return;
        }

        /* Проверка входа. Если пользователь не вошел, возвращаем страницу входа
         * Страницы для всех пользователей должны находиться выше этой проверки
         * Страницы для авторизованных пользователей должны находиться ниже этой проверки
         */
        if(!isset($_COOKIE['login']) || !isset($_COOKIE['hash'])) {
            require "views/UserAuth/login.php";
            return;
        } else {
            $model = Model::GetInstance();
            // Если куки были изменены вручную, то они удаляются и пользователя
            // перенаправляет на страницу входа
            if(!$model->CheckCookie($_COOKIE['login'], $_COOKIE['hash'])) {
                setcookie('login' ,"", time()-60*60*24*30, "/");
                setcookie('hash', "",  time()-60*60*24*30, "/");
                require "views/UserAuth/login.php";
                return;
            }
        }

        // "/news/{id}" route
        if(preg_match("/^news\/\d+$/", $path)) {
            $splitPath = preg_split('/\//',$path);
            $model = Model::GetInstance();
            $model->SetCurrentID($splitPath[1]);

            if($model->GetNewsByID($model->GetCurrentID()) != null) {
                require 'views/newsPage.php';
                return;
            }

            //если был передан несуществующий id новости, возрвращает 404
            require 'views/404.php';
            return;
        }

        // endpoint фильтра
        if(preg_match("/news\/filter*?/", $path)) {
            if($_GET['group'] != null && $_GET['group'] != 'all') $model->SetGroupPath($_GET['group']);
            require 'views/news.php';
            return;
        }

        // /news/ endpoint
        if(preg_match("/^news\/$/", $path)) {
            require 'views/news.php';
            return;
        }

        // news/group-[group_name] endpoint
        if(preg_match("/^news\/group-[a-zA-Z0-9]+$/", $path)) {
            $splitPath = preg_split('/-/',$path);
            $model = Model::GetInstance();

            $model->SetGroupPath($splitPath[1]);

            if($model->GetCurrentGroup() != null) {
                require 'views/group.php';
                return;

            }
            require 'views/404.php';
            return;
        }

        // news/group-[group_name]/[category_name] endpoint
        if(preg_match("/^news\/group-[a-zA-Z0-9]+\/[a-zA-Z0-9]+$/", $path)) {
            $splitPath = preg_split('/-/',$path);
            $splitPath = preg_split('/\//',$splitPath[1]);
            $model = Model::GetInstance();

            $model->SetGroupPath($splitPath[0]);
            $model->SetCurrentCategoryByPath($splitPath[1]);

            if($model->GetCurrentGroup() != null)
            {
                require 'views/category.php';
                return;
            }
            require 'views/404.php';
            return;
        }
        require_once 'views/404.php';
    }

}