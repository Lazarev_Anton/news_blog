-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 30 2021 г., 16:42
-- Версия сервера: 8.0.24
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `news_blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `Category_id` int NOT NULL,
  `name` varchar(40) NOT NULL,
  `path_name` varchar(40) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`Category_id`, `name`, `path_name`, `description`) VALUES
(1, 'Новое', 'new', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. In at mollis ex, sed fermentum lacus. Mauris imperdiet, diam vitae tempor blandit, tortor ipsum vulputate quam, nec facilisis.'),
(2, 'Актуальное', 'actual', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vestibulum est sed malesuada molestie. Vivamus ullamcorper mollis turpis, ac egestas orci tristique in. Fusce accumsan elementum est.'),
(3, 'Неактуальное', 'notactual', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vehicula lorem non velit venenatis feugiat. Praesent congue quis ante eget vehicula. Aliquam ultrices.'),
(4, 'Старое', 'old', 'Nunc dolor dui, venenatis vitae massa ac, consectetur aliquet magna. Sed elementum, urna vitae imperdiet laoreet, dolor elit vehicula velit, a maximus orci.'),
(5, 'Случайное', 'random', 'Pellentesque sed nunc dignissim, sodales lectus ut, sodales turpis. Donec dignissim quis ante eget fringilla. Quisque nec scelerisque justo, rhoncus sollicitudin dui. Vivamus tincidunt non justo non lobortis. Integer sit amet.');

-- --------------------------------------------------------

--
-- Структура таблицы `categories_news`
--

CREATE TABLE `categories_news` (
  `news_id` int NOT NULL,
  `category_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `categories_news`
--

INSERT INTO `categories_news` (`news_id`, `category_id`) VALUES
(3, 1),
(10, 1),
(13, 1),
(15, 1),
(16, 1),
(19, 1),
(1, 2),
(4, 2),
(5, 2),
(8, 2),
(9, 2),
(14, 2),
(16, 2),
(17, 2),
(18, 2),
(1, 3),
(3, 3),
(6, 3),
(9, 3),
(11, 3),
(12, 3),
(13, 3),
(18, 3),
(20, 3),
(2, 4),
(4, 4),
(5, 4),
(6, 4),
(10, 4),
(11, 4),
(12, 4),
(14, 4),
(20, 4),
(3, 5),
(7, 5),
(8, 5),
(15, 5),
(17, 5),
(19, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `group_id` int NOT NULL,
  `name` varchar(40) NOT NULL,
  `path_name` varchar(40) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`group_id`, `name`, `path_name`, `description`) VALUES
(1, 'Спорт', 'sport', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tortor sapien, accumsan ut mollis a, rhoncus vel elit. In quis hendrerit metus, id vestibulum risus.'),
(2, 'Искусство', 'art', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum in tellus nec nibh vulputate fermentum. Vivamus tincidunt nibh vel tellus cursus, nec porta dolor auctor. Ut sollicitudin scelerisque dui. Proin ac euismod.'),
(3, 'IT', 'it', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dapibus sapien in dui mattis sollicitudin. Morbi sodales dapibus');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `news_id` int NOT NULL,
  `news_name` varchar(40) NOT NULL,
  `pic_id` int NOT NULL,
  `group_id` int NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`news_id`, `news_name`, `pic_id`, `group_id`, `text`) VALUES
(1, 'Новость про кота', 1, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla lectus quis nibh elementum convallis. Vestibulum non tincidunt purus. Quisque tempus mattis augue sit amet fermentum. Nam eget mauris consequat neque efficitur faucibus. Integer mauris justo, eleifend a ligula quis, interdum ultricies ante. Curabitur congue faucibus enim. Fusce vitae velit magna. Fusce ac diam vel metus elementum vulputate eget id massa. Vestibulum fringilla neque eget velit porta, nec pretium risus volutpat. Sed bibendum justo vel odio elementum, non viverra turpis pellentesque. Duis sit amet commodo felis. Proin id nunc eu dolor consectetur vestibulum. Sed dapibus sapien ut commodo fermentum.\r\n\r\nDuis porttitor ipsum magna, ut bibendum massa ullamcorper tempus. Praesent ultrices aliquam lacus, et vulputate libero consequat sit amet. Aliquam erat volutpat. Curabitur ac risus vitae est ultricies venenatis. Nulla ut ex accumsan, tempus lorem vel, tincidunt nisi. Vestibulum rutrum, ligula et accumsan sollicitudin, leo eros interdum diam, vitae congue lorem sapien a nibh. Nulla eros eros, cursus a mi at, consequat accumsan lorem. Morbi convallis ullamcorper ultrices. Ut nisl ligula, tincidunt quis mi ac, tempor maximus eros. Donec et eros vitae diam lacinia volutpat. Proin et rutrum enim. Nam neque nunc, aliquam non pellentesque bibendum, tempor ut velit. Sed at accumsan leo. Pellentesque tincidunt mi at nisl rutrum aliquet. Sed porttitor eleifend risus eu scelerisque.\r\n\r\nCras id blandit dolor. Curabitur mollis pretium arcu, mattis euismod enim fermentum ac. Aliquam orci eros, pharetra quis viverra in, cursus fringilla libero. Etiam maximus nisi a ullamcorper imperdiet. Nullam viverra neque rhoncus urna finibus porta. Aenean sed vulputate mauris. Ut fermentum diam id pharetra aliquam. Quisque gravida, urna sit amet mollis auctor, massa turpis ultrices orci, vitae egestas turpis nisl sed urna. Suspendisse accumsan urna vitae ante luctus interdum. Nunc vel pulvinar ligula, vel ullamcorper elit. Donec sed augue non lacus scelerisque vehicula non ac mi. Fusce vitae rutrum neque. Ut tempus mi in arcu venenatis, at dictum quam malesuada. Fusce neque risus, aliquet non ex aliquam, vestibulum mattis neque.'),
(2, 'Новость про собаку', 2, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus condimentum elit eu mattis consequat. Suspendisse fermentum justo odio, a vehicula massa aliquet et. Nulla facilisis sagittis aliquet. Quisque vitae nulla vitae neque lacinia finibus. Sed id semper mi, ultricies egestas sapien. Vivamus faucibus libero vel justo vestibulum, sed scelerisque leo dapibus. Integer tincidunt dolor sapien, ut feugiat sapien bibendum vitae. Pellentesque lobortis porttitor venenatis. Donec erat lectus, posuere ac dignissim non, condimentum vitae augue. Maecenas iaculis, ex vel molestie elementum, diam justo sollicitudin est, sit amet lacinia magna justo ac ex. Morbi id tincidunt tellus, vel hendrerit dolor. Curabitur lorem nisl, facilisis et leo eget, aliquet viverra quam. Duis odio nibh, luctus et consequat ac, mattis eget elit.\r\n\r\nCras viverra velit vel dolor feugiat, et dignissim purus tincidunt. Curabitur ac quam eu augue gravida molestie eu ac lacus. Suspendisse eget facilisis est, eu varius nulla. Ut porta dignissim nisi in ornare. Proin consectetur faucibus velit, sed consequat arcu facilisis non. Donec volutpat lectus ut libero tempor euismod. Praesent mauris lacus, efficitur sit amet gravida vitae, dictum eget purus. Nunc eu condimentum nisi. Duis et nunc sagittis, dignissim augue sed, eleifend tellus. Proin nibh tortor, maximus et velit sed, placerat lacinia eros. Vestibulum consequat fermentum vulputate. Quisque sit amet est semper elit efficitur maximus. Mauris magna diam, laoreet vitae convallis ac, lobortis vitae ex. Nunc nec eleifend diam. Mauris in bibendum ligula.\r\n\r\nEtiam nulla nibh, convallis vel lobortis vel, tincidunt ac ligula. Phasellus pharetra mauris quis mi fringilla, vitae dapibus dui semper. Quisque commodo non lectus sed cursus. Duis a sapien iaculis, molestie libero aliquam, suscipit quam. Aliquam dapibus sit amet lacus sit amet lobortis. Etiam porttitor maximus libero vel sodales. Pellentesque facilisis sit amet neque in efficitur. Integer convallis tortor nec orci porttitor lacinia nec eget eros. Aliquam egestas neque nec purus viverra, sed porta neque porta. Suspendisse placerat, lacus sit amet ultricies suscipit, nibh dolor venenatis mi, et venenatis dolor diam nec arcu.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque dictum hendrerit magna, ac ultricies urna mollis ac. Mauris et nisl felis. Vivamus convallis sagittis mi. Fusce est erat, mattis id posuere a, sagittis eu leo. Cras ac feugiat augue. Nullam felis ex, efficitur eu nunc id, lobortis tristique ligula. Proin tempor blandit lacus, nec iaculis lectus sagittis ut. Proin convallis dolor sit amet vestibulum vulputate. In facilisis euismod enim, nec posuere odio maximus eu. Nulla magna elit, tempor id imperdiet sit amet, dignissim a turpis.\r\n\r\nMaecenas viverra est auctor turpis tempus porta in vel lacus. Duis nisl ipsum, suscipit gravida lacinia at, tincidunt sit amet neque. Nam facilisis nisi ut laoreet eleifend. Morbi tincidunt at nisl vel sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce eu sapien sollicitudin, pretium magna id, iaculis lectus. Integer maximus ultricies justo, vitae mattis tellus vestibulum et. Proin a leo est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas ut ligula gravida, mollis justo quis, volutpat tortor. Suspendisse potenti.'),
(3, 'Lorem Ipsum', 3, 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ut odio id nunc posuere volutpat eget maximus lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas porttitor elementum leo, ac ultricies nunc sagittis vitae. Vestibulum ac ligula sit amet velit faucibus blandit in vitae massa. Etiam vestibulum efficitur elit, in pharetra arcu maximus sit amet. Proin fringilla sit amet libero nec fermentum. Aenean facilisis sapien nec tortor hendrerit, at iaculis elit aliquam. Maecenas finibus sodales pharetra. Cras pulvinar sit amet ante ac dapibus.\r\n\r\nVestibulum sollicitudin massa et urna sollicitudin placerat. Vestibulum id lacus in ex luctus accumsan sit amet vel metus. Proin at lacus ante. Integer aliquet massa pharetra felis tristique lacinia. Ut condimentum aliquam erat, non efficitur mauris maximus et. Suspendisse potenti. Aenean hendrerit pulvinar massa a pulvinar. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi et felis rutrum magna efficitur mollis nec sit amet neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),
(4, 'Lorem Ipsum 2', 4, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta nisl ac augue condimentum cursus. Morbi mollis tincidunt nibh commodo interdum. Nulla semper facilisis fermentum. Duis id felis neque. Nulla non viverra metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec lorem nec turpis blandit condimentum a sit amet purus. Mauris dictum volutpat rhoncus.\r\n\r\nQuisque gravida, ipsum pharetra gravida eleifend, enim sem scelerisque tortor, et lacinia elit leo ac enim. Maecenas convallis felis lacus, eget eleifend ligula laoreet quis. Quisque nec diam sed mi posuere eleifend. Praesent at nisi aliquet, rhoncus erat sed, facilisis erat. Sed vitae luctus nunc, a sagittis risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris posuere eros et lorem eleifend, sit amet ornare quam placerat. Donec eu tincidunt neque. Duis finibus tempus iaculis. Aenean nec pharetra velit, non vulputate tortor. Maecenas molestie sapien ac elit hendrerit, sit amet ultricies nibh rutrum. In faucibus purus sit amet elementum efficitur.\r\n\r\nDonec bibendum augue est, ac rhoncus felis aliquet sit amet. Praesent eget pretium lacus, sed maximus orci. Nulla placerat nibh in fringilla varius. Sed eget facilisis diam. Mauris hendrerit sapien quis lorem consequat, a luctus nisl consequat. Ut mollis vestibulum arcu ut dignissim. In gravida egestas gravida. Cras a congue ex, nec vestibulum diam. Aliquam iaculis magna non tortor lacinia, nec dignissim nisi efficitur. Morbi et nibh eu dolor cursus aliquam. Ut imperdiet dictum ligula, volutpat posuere turpis faucibus ac. Pellentesque sed iaculis elit. Proin vehicula eget eros eget lacinia.'),
(5, 'Lorem ipsum dolor sit', 5, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta nisl ac augue condimentum cursus. Morbi mollis tincidunt nibh commodo interdum. Nulla semper facilisis fermentum. Duis id felis neque. Nulla non viverra metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec lorem nec turpis blandit condimentum a sit amet purus. Mauris dictum volutpat rhoncus.\r\n\r\nQuisque gravida, ipsum pharetra gravida eleifend, enim sem scelerisque tortor, et lacinia elit leo ac enim. Maecenas convallis felis lacus, eget eleifend ligula laoreet quis. Quisque nec diam sed mi posuere eleifend. Praesent at nisi aliquet, rhoncus erat sed, facilisis erat. Sed vitae luctus nunc, a sagittis risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris posuere eros et lorem eleifend, sit amet ornare quam placerat. Donec eu tincidunt neque. Duis finibus tempus iaculis. Aenean nec pharetra velit, non vulputate tortor. Maecenas molestie sapien ac elit hendrerit, sit amet ultricies nibh rutrum. In faucibus purus sit amet elementum efficitur.\r\n\r\nDonec bibendum augue est, ac rhoncus felis aliquet sit amet. Praesent eget pretium lacus, sed maximus orci. Nulla placerat nibh in fringilla varius. Sed eget facilisis diam. Mauris hendrerit sapien quis lorem consequat, a luctus nisl consequat. Ut mollis vestibulum arcu ut dignissim. In gravida egestas gravida. Cras a congue ex, nec vestibulum diam. Aliquam iaculis magna non tortor lacinia, nec dignissim nisi efficitur. Morbi et nibh eu dolor cursus aliquam. Ut imperdiet dictum ligula, volutpat posuere turpis faucibus ac. Pellentesque sed iaculis elit. Proin vehicula eget eros eget lacinia.'),
(6, 'NOVOST\' PRO MULT', 6, 2, '\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta nisl ac augue condimentum cursus. Morbi mollis tincidunt nibh commodo interdum. Nulla semper facilisis fermentum. Duis id felis neque. Nulla non viverra metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec lorem nec turpis blandit condimentum a sit amet purus. Mauris dictum volutpat rhoncus.\r\n\r\nQuisque gravida, ipsum pharetra gravida eleifend, enim sem scelerisque tortor, et lacinia elit leo ac enim. Maecenas convallis felis lacus, eget eleifend ligula laoreet quis. Quisque nec diam sed mi posuere eleifend. Praesent at nisi aliquet, rhoncus erat sed, facilisis erat. Sed vitae luctus nunc, a sagittis risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris posuere eros et lorem eleifend, sit amet ornare quam placerat. Donec eu tincidunt neque. Duis finibus tempus iaculis. Aenean nec pharetra velit, non vulputate tortor. Maecenas molestie sapien ac elit hendrerit, sit amet ultricies nibh rutrum. In faucibus purus sit amet elementum efficitur.'),
(7, 'Donec bibendum augue est', 3, 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse semper egestas nibh, vitae rutrum odio ultricies ut. Nam augue ligula, ullamcorper et vestibulum in, euismod sit amet urna. Donec luctus ante in blandit tincidunt. Proin vitae turpis suscipit, consequat orci et, imperdiet nisi. Nunc felis justo, tristique id luctus in, pellentesque eget tellus. Proin laoreet leo sapien, vel posuere massa blandit non. Quisque et diam sem. Nam tristique accumsan risus vel congue. Aenean luctus congue lacus, quis consequat mi. Ut iaculis, purus dignissim ultricies blandit, erat magna volutpat enim, in semper est dolor bibendum lorem. In mollis vel lectus eu venenatis. Fusce viverra metus vel nisi sagittis porta. In viverra vel turpis at ornare.\r\n\r\nDonec sed sollicitudin risus. Curabitur dapibus enim porta ipsum semper tempus. Morbi pulvinar tellus dolor, et vulputate leo ornare et. Vestibulum nec sem feugiat turpis facilisis pretium non dignissim ligula. Maecenas vehicula lacinia purus quis porttitor. Nunc et leo fermentum, dignissim metus eget, pellentesque tellus. Mauris vel eleifend enim.\r\n\r\nMauris viverra arcu ut est egestas, porttitor dapibus diam congue. Etiam feugiat, magna nec facilisis interdum, ligula tortor malesuada felis, nec cursus odio ex non nisl. Sed eget lacinia est, at fringilla enim. Fusce malesuada sem nec egestas feugiat. Aliquam varius elementum placerat. Duis a pellentesque orci. Quisque luctus commodo magna, in laoreet enim consequat eget. Proin eu ullamcorper justo. Proin et lorem leo.\r\n\r\nNulla et consectetur justo. Nam convallis et lectus sed hendrerit. Etiam purus quam, fermentum sit amet turpis eu, luctus gravida magna. Maecenas a mattis ante, in efficitur turpis. Suspendisse egestas lacinia metus, vitae sodales mauris porta et. Suspendisse ac purus feugiat nisi molestie malesuada. Aenean at ligula eget est fermentum sodales. Mauris faucibus nisi consectetur est congue, nec condimentum eros eleifend. Proin porta justo nunc, et lacinia erat maximus sit amet. Morbi luctus quis orci eu vehicula. Nulla facilisi. Donec tempor leo a commodo imperdiet.\r\n\r\nDonec ultricies lacus a nulla sollicitudin pharetra. Sed vitae lectus quis felis bibendum faucibus vestibulum sit amet diam. Nunc elementum vestibulum felis et iaculis. Quisque placerat orci vel dolor fermentum aliquet. Nullam sodales elementum sapien in tempor. Proin tortor tellus, feugiat in fermentum a, pretium quis tellus. Nullam sodales aliquam nulla, sit amet pharetra metus sagittis non. In quis diam vitae nisi ullamcorper semper eu sit amet lectus. Nullam vitae sagittis risus. Curabitur ut libero vulputate augue vestibulum tincidunt in vitae felis. Pellentesque gravida convallis enim sed imperdiet. Vestibulum et pulvinar nisl. Sed est massa, tristique ac velit eget, volutpat ullamcorper dolor. Nam molestie augue at pulvinar posuere.'),
(8, 'Lorem Ipsum', 2, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta nisl ac augue condimentum cursus. Morbi mollis tincidunt nibh commodo interdum. Nulla semper facilisis fermentum. Duis id felis neque. Nulla non viverra metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec lorem nec turpis blandit condimentum a sit amet purus. Mauris dictum volutpat rhoncus.\r\n\r\nQuisque gravida, ipsum pharetra gravida eleifend, enim sem scelerisque tortor, et lacinia elit leo ac enim. Maecenas convallis felis lacus, eget eleifend ligula laoreet quis. Quisque nec diam sed mi posuere eleifend. Praesent at nisi aliquet, rhoncus erat sed, facilisis erat. Sed vitae luctus nunc, a sagittis risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris posuere eros et lorem eleifend, sit amet ornare quam placerat. Donec eu tincidunt neque. Duis finibus tempus iaculis. Aenean nec pharetra velit, non vulputate tortor. Maecenas molestie sapien ac elit hendrerit, sit amet ultricies nibh rutrum. In faucibus purus sit amet elementum efficitur.\r\n\r\nDonec bibendum augue est, ac rhoncus felis aliquet sit amet. Praesent eget pretium lacus, sed maximus orci. Nulla placerat nibh in fringilla varius. Sed eget facilisis diam. Mauris hendrerit sapien quis lorem consequat, a luctus nisl consequat. Ut mollis vestibulum arcu ut dignissim. In gravida egestas gravida. Cras a congue ex, nec vestibulum diam. Aliquam iaculis magna non tortor lacinia, nec dignissim nisi efficitur. Morbi et nibh eu dolor cursus aliquam. Ut imperdiet dictum ligula, volutpat posuere turpis faucibus ac. Pellentesque sed iaculis elit. Proin vehicula eget eros eget lacinia.'),
(9, 'Lorem Ipsum', 5, 2, '\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta nisl ac augue condimentum cursus. Morbi mollis tincidunt nibh commodo interdum. Nulla semper facilisis fermentum. Duis id felis neque. Nulla non viverra metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec lorem nec turpis blandit condimentum a sit amet purus. Mauris dictum volutpat rhoncus.\r\n\r\nQuisque gravida, ipsum pharetra gravida eleifend, enim sem scelerisque tortor, et lacinia elit leo ac enim. Maecenas convallis felis lacus, eget eleifend ligula laoreet quis. Quisque nec diam sed mi posuere eleifend. Praesent at nisi aliquet, rhoncus erat sed, facilisis erat. Sed vitae luctus nunc, a sagittis risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris posuere eros et lorem eleifend, sit amet ornare quam placerat. Donec eu tincidunt neque. Duis finibus tempus iaculis. Aenean nec pharetra velit, non vulputate tortor. Maecenas molestie sapien ac elit hendrerit, sit amet ultricies nibh rutrum. In faucibus purus sit amet elementum efficitur.'),
(10, 'Lorem Ipsum', 3, 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse semper egestas nibh, vitae rutrum odio ultricies ut. Nam augue ligula, ullamcorper et vestibulum in, euismod sit amet urna. Donec luctus ante in blandit tincidunt. Proin vitae turpis suscipit, consequat orci et, imperdiet nisi. Nunc felis justo, tristique id luctus in, pellentesque eget tellus. Proin laoreet leo sapien, vel posuere massa blandit non. Quisque et diam sem. Nam tristique accumsan risus vel congue. Aenean luctus congue lacus, quis consequat mi. Ut iaculis, purus dignissim ultricies blandit, erat magna volutpat enim, in semper est dolor bibendum lorem. In mollis vel lectus eu venenatis. Fusce viverra metus vel nisi sagittis porta. In viverra vel turpis at ornare.\r\n\r\nDonec sed sollicitudin risus. Curabitur dapibus enim porta ipsum semper tempus. Morbi pulvinar tellus dolor, et vulputate leo ornare et. Vestibulum nec sem feugiat turpis facilisis pretium non dignissim ligula. Maecenas vehicula lacinia purus quis porttitor. Nunc et leo fermentum, dignissim metus eget, pellentesque tellus. Mauris vel eleifend enim.\r\n\r\nMauris viverra arcu ut est egestas, porttitor dapibus diam congue. Etiam feugiat, magna nec facilisis interdum, ligula tortor malesuada felis, nec cursus odio ex non nisl. Sed eget lacinia est, at fringilla enim. Fusce malesuada sem nec egestas feugiat. Aliquam varius elementum placerat. Duis a pellentesque orci. Quisque luctus commodo magna, in laoreet enim consequat eget. Proin eu ullamcorper justo. Proin et lorem leo.\r\n\r\nNulla et consectetur justo. Nam convallis et lectus sed hendrerit. Etiam purus quam, fermentum sit amet turpis eu, luctus gravida magna. Maecenas a mattis ante, in efficitur turpis. Suspendisse egestas lacinia metus, vitae sodales mauris porta et. Suspendisse ac purus feugiat nisi molestie malesuada. Aenean at ligula eget est fermentum sodales. Mauris faucibus nisi consectetur est congue, nec condimentum eros eleifend. Proin porta justo nunc, et lacinia erat maximus sit amet. Morbi luctus quis orci eu vehicula. Nulla facilisi. Donec tempor leo a commodo imperdiet.\r\n\r\nDonec ultricies lacus a nulla sollicitudin pharetra. Sed vitae lectus quis felis bibendum faucibus vestibulum sit amet diam. Nunc elementum vestibulum felis et iaculis. Quisque placerat orci vel dolor fermentum aliquet. Nullam sodales elementum sapien in tempor. Proin tortor tellus, feugiat in fermentum a, pretium quis tellus. Nullam sodales aliquam nulla, sit amet pharetra metus sagittis non. In quis diam vitae nisi ullamcorper semper eu sit amet lectus. Nullam vitae sagittis risus. Curabitur ut libero vulputate augue vestibulum tincidunt in vitae felis. Pellentesque gravida convallis enim sed imperdiet. Vestibulum et pulvinar nisl. Sed est massa, tristique ac velit eget, volutpat ullamcorper dolor. Nam molestie augue at pulvinar posuere.'),
(11, 'Lorem Ipsum', 4, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse semper egestas nibh, vitae rutrum odio ultricies ut. Nam augue ligula, ullamcorper et vestibulum in, euismod sit amet urna. Donec luctus ante in blandit tincidunt. Proin vitae turpis suscipit, consequat orci et, imperdiet nisi. Nunc felis justo, tristique id luctus in, pellentesque eget tellus. Proin laoreet leo sapien, vel posuere massa blandit non. Quisque et diam sem. Nam tristique accumsan risus vel congue. Aenean luctus congue lacus, quis consequat mi. Ut iaculis, purus dignissim ultricies blandit, erat magna volutpat enim, in semper est dolor bibendum lorem. In mollis vel lectus eu venenatis. Fusce viverra metus vel nisi sagittis porta. In viverra vel turpis at ornare.'),
(12, 'Lorem Ipsum', 2, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta nisl ac augue condimentum cursus. Morbi mollis tincidunt nibh commodo interdum. Nulla semper facilisis fermentum. Duis id felis neque. Nulla non viverra metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec lorem nec turpis blandit condimentum a sit amet purus. Mauris dictum volutpat rhoncus.\r\n\r\nQuisque gravida, ipsum pharetra gravida eleifend, enim sem scelerisque tortor, et lacinia elit leo ac enim. Maecenas convallis felis lacus, eget eleifend ligula laoreet quis. Quisque nec diam sed mi posuere eleifend. Praesent at nisi aliquet, rhoncus erat sed, facilisis erat. Sed vitae luctus nunc, a sagittis risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris posuere eros et lorem eleifend, sit amet ornare quam placerat. Donec eu tincidunt neque. Duis finibus tempus iaculis. Aenean nec pharetra velit, non vulputate tortor. Maecenas molestie sapien ac elit hendrerit, sit amet ultricies nibh rutrum. In faucibus purus sit amet elementum efficitur.\r\n\r\nDonec bibendum augue est, ac rhoncus felis aliquet sit amet. Praesent eget pretium lacus, sed maximus orci. Nulla placerat nibh in fringilla varius. Sed eget facilisis diam. Mauris hendrerit sapien quis lorem consequat, a luctus nisl consequat. Ut mollis vestibulum arcu ut dignissim. In gravida egestas gravida. Cras a congue ex, nec vestibulum diam. Aliquam iaculis magna non tortor lacinia, nec dignissim nisi efficitur. Morbi et nibh eu dolor cursus aliquam. Ut imperdiet dictum ligula, volutpat posuere turpis faucibus ac. Pellentesque sed iaculis elit. Proin vehicula eget eros eget lacinia.'),
(13, 'Lorem Ipsum', 4, 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse semper egestas nibh, vitae rutrum odio ultricies ut. Nam augue ligula, ullamcorper et vestibulum in, euismod sit amet urna. Donec luctus ante in blandit tincidunt. Proin vitae turpis suscipit, consequat orci et, imperdiet nisi. Nunc felis justo, tristique id luctus in, pellentesque eget tellus. Proin laoreet leo sapien, vel posuere massa blandit non. Quisque et diam sem. Nam tristique accumsan risus vel congue. Aenean luctus congue lacus, quis consequat mi. Ut iaculis, purus dignissim ultricies blandit, erat magna volutpat enim, in semper est dolor bibendum lorem. In mollis vel lectus eu venenatis. Fusce viverra metus vel nisi sagittis porta. In viverra vel turpis at ornare.\r\n\r\nDonec sed sollicitudin risus. Curabitur dapibus enim porta ipsum semper tempus. Morbi pulvinar tellus dolor, et vulputate leo ornare et. Vestibulum nec sem feugiat turpis facilisis pretium non dignissim ligula. Maecenas vehicula lacinia purus quis porttitor. Nunc et leo fermentum, dignissim metus eget, pellentesque tellus. Mauris vel eleifend enim.\r\n\r\nMauris viverra arcu ut est egestas, porttitor dapibus diam congue. Etiam feugiat, magna nec facilisis interdum, ligula tortor malesuada felis, nec cursus odio ex non nisl. Sed eget lacinia est, at fringilla enim. Fusce malesuada sem nec egestas feugiat. Aliquam varius elementum placerat. Duis a pellentesque orci. Quisque luctus commodo magna, in laoreet enim consequat eget. Proin eu ullamcorper justo. Proin et lorem leo.\r\n\r\nNulla et consectetur justo. Nam convallis et lectus sed hendrerit. Etiam purus quam, fermentum sit amet turpis eu, luctus gravida magna. Maecenas a mattis ante, in efficitur turpis. Suspendisse egestas lacinia metus, vitae sodales mauris porta et. Suspendisse ac purus feugiat nisi molestie malesuada. Aenean at ligula eget est fermentum sodales. Mauris faucibus nisi consectetur est congue, nec condimentum eros eleifend. Proin porta justo nunc, et lacinia erat maximus sit amet. Morbi luctus quis orci eu vehicula. Nulla facilisi. Donec tempor leo a commodo imperdiet.\r\n\r\nDonec ultricies lacus a nulla sollicitudin pharetra. Sed vitae lectus quis felis bibendum faucibus vestibulum sit amet diam. Nunc elementum vestibulum felis et iaculis. Quisque placerat orci vel dolor fermentum aliquet. Nullam sodales elementum sapien in tempor. Proin tortor tellus, feugiat in fermentum a, pretium quis tellus. Nullam sodales aliquam nulla, sit amet pharetra metus sagittis non. In quis diam vitae nisi ullamcorper semper eu sit amet lectus. Nullam vitae sagittis risus. Curabitur ut libero vulputate augue vestibulum tincidunt in vitae felis. Pellentesque gravida convallis enim sed imperdiet. Vestibulum et pulvinar nisl. Sed est massa, tristique ac velit eget, volutpat ullamcorper dolor. Nam molestie augue at pulvinar posuere.'),
(14, 'Lorem Ipsum', 1, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse semper egestas nibh, vitae rutrum odio ultricies ut. Nam augue ligula, ullamcorper et vestibulum in, euismod sit amet urna. Donec luctus ante in blandit tincidunt. Proin vitae turpis suscipit, consequat orci et, imperdiet nisi. Nunc felis justo, tristique id luctus in, pellentesque eget tellus. Proin laoreet leo sapien, vel posuere massa blandit non. Quisque et diam sem. Nam tristique accumsan risus vel congue. Aenean luctus congue lacus, quis consequat mi. Ut iaculis, purus dignissim ultricies blandit, erat magna volutpat enim, in semper est dolor bibendum lorem. In mollis vel lectus eu venenatis. Fusce viverra metus vel nisi sagittis porta. In viverra vel turpis at ornare.'),
(15, 'Lorem Ipsum', 2, 1, '\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta nisl ac augue condimentum cursus. Morbi mollis tincidunt nibh commodo interdum. Nulla semper facilisis fermentum. Duis id felis neque. Nulla non viverra metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec lorem nec turpis blandit condimentum a sit amet purus. Mauris dictum volutpat rhoncus.\r\n\r\nQuisque gravida, ipsum pharetra gravida eleifend, enim sem scelerisque tortor, et lacinia elit leo ac enim. Maecenas convallis felis lacus, eget eleifend ligula laoreet quis. Quisque nec diam sed mi posuere eleifend. Praesent at nisi aliquet, rhoncus erat sed, facilisis erat. Sed vitae luctus nunc, a sagittis risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris posuere eros et lorem eleifend, sit amet ornare quam placerat. Donec eu tincidunt neque. Duis finibus tempus iaculis. Aenean nec pharetra velit, non vulputate tortor. Maecenas molestie sapien ac elit hendrerit, sit amet ultricies nibh rutrum. In faucibus purus sit amet elementum efficitur.'),
(16, 'Lorem Ipsum', 4, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta nisl ac augue condimentum cursus. Morbi mollis tincidunt nibh commodo interdum. Nulla semper facilisis fermentum. Duis id felis neque. Nulla non viverra metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec lorem nec turpis blandit condimentum a sit amet purus. Mauris dictum volutpat rhoncus.\r\n\r\nQuisque gravida, ipsum pharetra gravida eleifend, enim sem scelerisque tortor, et lacinia elit leo ac enim. Maecenas convallis felis lacus, eget eleifend ligula laoreet quis. Quisque nec diam sed mi posuere eleifend. Praesent at nisi aliquet, rhoncus erat sed, facilisis erat. Sed vitae luctus nunc, a sagittis risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris posuere eros et lorem eleifend, sit amet ornare quam placerat. Donec eu tincidunt neque. Duis finibus tempus iaculis. Aenean nec pharetra velit, non vulputate tortor. Maecenas molestie sapien ac elit hendrerit, sit amet ultricies nibh rutrum. In faucibus purus sit amet elementum efficitur.\r\n\r\nDonec bibendum augue est, ac rhoncus felis aliquet sit amet. Praesent eget pretium lacus, sed maximus orci. Nulla placerat nibh in fringilla varius. Sed eget facilisis diam. Mauris hendrerit sapien quis lorem consequat, a luctus nisl consequat. Ut mollis vestibulum arcu ut dignissim. In gravida egestas gravida. Cras a congue ex, nec vestibulum diam. Aliquam iaculis magna non tortor lacinia, nec dignissim nisi efficitur. Morbi et nibh eu dolor cursus aliquam. Ut imperdiet dictum ligula, volutpat posuere turpis faucibus ac. Pellentesque sed iaculis elit. Proin vehicula eget eros eget lacinia.'),
(17, 'Lorem Ipsum', 5, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse semper egestas nibh, vitae rutrum odio ultricies ut. Nam augue ligula, ullamcorper et vestibulum in, euismod sit amet urna. Donec luctus ante in blandit tincidunt. Proin vitae turpis suscipit, consequat orci et, imperdiet nisi. Nunc felis justo, tristique id luctus in, pellentesque eget tellus. Proin laoreet leo sapien, vel posuere massa blandit non. Quisque et diam sem. Nam tristique accumsan risus vel congue. Aenean luctus congue lacus, quis consequat mi. Ut iaculis, purus dignissim ultricies blandit, erat magna volutpat enim, in semper est dolor bibendum lorem. In mollis vel lectus eu venenatis. Fusce viverra metus vel nisi sagittis porta. In viverra vel turpis at ornare.\r\n\r\nDonec sed sollicitudin risus. Curabitur dapibus enim porta ipsum semper tempus. Morbi pulvinar tellus dolor, et vulputate leo ornare et. Vestibulum nec sem feugiat turpis facilisis pretium non dignissim ligula. Maecenas vehicula lacinia purus quis porttitor. Nunc et leo fermentum, dignissim metus eget, pellentesque tellus. Mauris vel eleifend enim.\r\n\r\nMauris viverra arcu ut est egestas, porttitor dapibus diam congue. Etiam feugiat, magna nec facilisis interdum, ligula tortor malesuada felis, nec cursus odio ex non nisl. Sed eget lacinia est, at fringilla enim. Fusce malesuada sem nec egestas feugiat. Aliquam varius elementum placerat. Duis a pellentesque orci. Quisque luctus commodo magna, in laoreet enim consequat eget. Proin eu ullamcorper justo. Proin et lorem leo.\r\n\r\nNulla et consectetur justo. Nam convallis et lectus sed hendrerit. Etiam purus quam, fermentum sit amet turpis eu, luctus gravida magna. Maecenas a mattis ante, in efficitur turpis. Suspendisse egestas lacinia metus, vitae sodales mauris porta et. Suspendisse ac purus feugiat nisi molestie malesuada. Aenean at ligula eget est fermentum sodales. Mauris faucibus nisi consectetur est congue, nec condimentum eros eleifend. Proin porta justo nunc, et lacinia erat maximus sit amet. Morbi luctus quis orci eu vehicula. Nulla facilisi. Donec tempor leo a commodo imperdiet.\r\n\r\nDonec ultricies lacus a nulla sollicitudin pharetra. Sed vitae lectus quis felis bibendum faucibus vestibulum sit amet diam. Nunc elementum vestibulum felis et iaculis. Quisque placerat orci vel dolor fermentum aliquet. Nullam sodales elementum sapien in tempor. Proin tortor tellus, feugiat in fermentum a, pretium quis tellus. Nullam sodales aliquam nulla, sit amet pharetra metus sagittis non. In quis diam vitae nisi ullamcorper semper eu sit amet lectus. Nullam vitae sagittis risus. Curabitur ut libero vulputate augue vestibulum tincidunt in vitae felis. Pellentesque gravida convallis enim sed imperdiet. Vestibulum et pulvinar nisl. Sed est massa, tristique ac velit eget, volutpat ullamcorper dolor. Nam molestie augue at pulvinar posuere.'),
(18, 'Lorem Ipsum', 6, 3, '\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta nisl ac augue condimentum cursus. Morbi mollis tincidunt nibh commodo interdum. Nulla semper facilisis fermentum. Duis id felis neque. Nulla non viverra metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec lorem nec turpis blandit condimentum a sit amet purus. Mauris dictum volutpat rhoncus.\r\n\r\nQuisque gravida, ipsum pharetra gravida eleifend, enim sem scelerisque tortor, et lacinia elit leo ac enim. Maecenas convallis felis lacus, eget eleifend ligula laoreet quis. Quisque nec diam sed mi posuere eleifend. Praesent at nisi aliquet, rhoncus erat sed, facilisis erat. Sed vitae luctus nunc, a sagittis risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris posuere eros et lorem eleifend, sit amet ornare quam placerat. Donec eu tincidunt neque. Duis finibus tempus iaculis. Aenean nec pharetra velit, non vulputate tortor. Maecenas molestie sapien ac elit hendrerit, sit amet ultricies nibh rutrum. In faucibus purus sit amet elementum efficitur.'),
(19, 'Lorem Ipsum', 3, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse semper egestas nibh, vitae rutrum odio ultricies ut. Nam augue ligula, ullamcorper et vestibulum in, euismod sit amet urna. Donec luctus ante in blandit tincidunt. Proin vitae turpis suscipit, consequat orci et, imperdiet nisi. Nunc felis justo, tristique id luctus in, pellentesque eget tellus. Proin laoreet leo sapien, vel posuere massa blandit non. Quisque et diam sem. Nam tristique accumsan risus vel congue. Aenean luctus congue lacus, quis consequat mi. Ut iaculis, purus dignissim ultricies blandit, erat magna volutpat enim, in semper est dolor bibendum lorem. In mollis vel lectus eu venenatis. Fusce viverra metus vel nisi sagittis porta. In viverra vel turpis at ornare.\r\n\r\nDonec sed sollicitudin risus. Curabitur dapibus enim porta ipsum semper tempus. Morbi pulvinar tellus dolor, et vulputate leo ornare et. Vestibulum nec sem feugiat turpis facilisis pretium non dignissim ligula. Maecenas vehicula lacinia purus quis porttitor. Nunc et leo fermentum, dignissim metus eget, pellentesque tellus. Mauris vel eleifend enim.\r\n\r\nMauris viverra arcu ut est egestas, porttitor dapibus diam congue. Etiam feugiat, magna nec facilisis interdum, ligula tortor malesuada felis, nec cursus odio ex non nisl. Sed eget lacinia est, at fringilla enim. Fusce malesuada sem nec egestas feugiat. Aliquam varius elementum placerat. Duis a pellentesque orci. Quisque luctus commodo magna, in laoreet enim consequat eget. Proin eu ullamcorper justo. Proin et lorem leo.\r\n\r\nNulla et consectetur justo. Nam convallis et lectus sed hendrerit. Etiam purus quam, fermentum sit amet turpis eu, luctus gravida magna. Maecenas a mattis ante, in efficitur turpis. Suspendisse egestas lacinia metus, vitae sodales mauris porta et. Suspendisse ac purus feugiat nisi molestie malesuada. Aenean at ligula eget est fermentum sodales. Mauris faucibus nisi consectetur est congue, nec condimentum eros eleifend. Proin porta justo nunc, et lacinia erat maximus sit amet. Morbi luctus quis orci eu vehicula. Nulla facilisi. Donec tempor leo a commodo imperdiet.\r\n\r\nDonec ultricies lacus a nulla sollicitudin pharetra. Sed vitae lectus quis felis bibendum faucibus vestibulum sit amet diam. Nunc elementum vestibulum felis et iaculis. Quisque placerat orci vel dolor fermentum aliquet. Nullam sodales elementum sapien in tempor. Proin tortor tellus, feugiat in fermentum a, pretium quis tellus. Nullam sodales aliquam nulla, sit amet pharetra metus sagittis non. In quis diam vitae nisi ullamcorper semper eu sit amet lectus. Nullam vitae sagittis risus. Curabitur ut libero vulputate augue vestibulum tincidunt in vitae felis. Pellentesque gravida convallis enim sed imperdiet. Vestibulum et pulvinar nisl. Sed est massa, tristique ac velit eget, volutpat ullamcorper dolor. Nam molestie augue at pulvinar posuere.'),
(20, 'Lorem Ipsum', 2, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta nisl ac augue condimentum cursus. Morbi mollis tincidunt nibh commodo interdum. Nulla semper facilisis fermentum. Duis id felis neque. Nulla non viverra metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec lorem nec turpis blandit condimentum a sit amet purus. Mauris dictum volutpat rhoncus.\r\n\r\nQuisque gravida, ipsum pharetra gravida eleifend, enim sem scelerisque tortor, et lacinia elit leo ac enim. Maecenas convallis felis lacus, eget eleifend ligula laoreet quis. Quisque nec diam sed mi posuere eleifend. Praesent at nisi aliquet, rhoncus erat sed, facilisis erat. Sed vitae luctus nunc, a sagittis risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris posuere eros et lorem eleifend, sit amet ornare quam placerat. Donec eu tincidunt neque. Duis finibus tempus iaculis. Aenean nec pharetra velit, non vulputate tortor. Maecenas molestie sapien ac elit hendrerit, sit amet ultricies nibh rutrum. In faucibus purus sit amet elementum efficitur.\r\n\r\nDonec bibendum augue est, ac rhoncus felis aliquet sit amet. Praesent eget pretium lacus, sed maximus orci. Nulla placerat nibh in fringilla varius. Sed eget facilisis diam. Mauris hendrerit sapien quis lorem consequat, a luctus nisl consequat. Ut mollis vestibulum arcu ut dignissim. In gravida egestas gravida. Cras a congue ex, nec vestibulum diam. Aliquam iaculis magna non tortor lacinia, nec dignissim nisi efficitur. Morbi et nibh eu dolor cursus aliquam. Ut imperdiet dictum ligula, volutpat posuere turpis faucibus ac. Pellentesque sed iaculis elit. Proin vehicula eget eros eget lacinia.');

-- --------------------------------------------------------

--
-- Структура таблицы `picturies`
--

CREATE TABLE `picturies` (
  `pic_id` int NOT NULL,
  `alt` varchar(40) NOT NULL,
  `path` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `picturies`
--

INSERT INTO `picturies` (`pic_id`, `alt`, `path`) VALUES
(1, 'котик', 'cat.jpg'),
(2, 'Хороший мальчик', 'dog.jpg'),
(3, 'Слон', 'elephant.jpg'),
(4, 'Машина', 'car.jpg'),
(5, 'улитка', 'snail.jpg'),
(6, 'Мультик', 'toon.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `user_id` int NOT NULL,
  `login` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `login`, `password`) VALUES
(13, 'user', 'ed13fb8d4be5022185882b578e1ef60f'),
(14, 'user2', 'd74f4b043c60b16739571ba8cd449719'),
(15, '222', 'be42cb6802c4fe29e300a318a3e40d4f');

-- --------------------------------------------------------

--
-- Структура таблицы `users_views`
--

CREATE TABLE `users_views` (
  `news_id` int NOT NULL,
  `user_id` int NOT NULL,
  `view_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `users_views`
--

INSERT INTO `users_views` (`news_id`, `user_id`, `view_date`) VALUES
(1, 13, '2021-09-30 13:09:18'),
(1, 13, '2021-09-30 13:09:20'),
(1, 13, '2021-09-30 13:09:21'),
(1, 13, '2021-09-30 13:09:31'),
(1, 13, '2021-09-30 13:09:34'),
(1, 13, '2021-09-30 13:09:35'),
(4, 13, '2021-09-30 13:09:04'),
(4, 13, '2021-09-30 13:09:07'),
(4, 13, '2021-09-30 13:09:08'),
(4, 13, '2021-09-30 13:09:09'),
(4, 13, '2021-09-30 13:12:33'),
(4, 13, '2021-09-30 13:12:36'),
(4, 13, '2021-09-30 13:12:47'),
(4, 13, '2021-09-30 13:24:52'),
(4, 13, '2021-09-30 13:25:01'),
(4, 13, '2021-09-30 13:26:45'),
(4, 13, '2021-09-30 13:32:14'),
(4, 13, '2021-09-30 13:34:18'),
(4, 13, '2021-09-30 13:34:22'),
(4, 13, '2021-09-30 13:38:39'),
(4, 13, '2021-09-30 13:40:01'),
(4, 13, '2021-09-30 13:42:24'),
(6, 13, '2021-09-30 13:40:28'),
(6, 13, '2021-09-30 13:40:30'),
(6, 13, '2021-09-30 13:40:31'),
(6, 13, '2021-09-30 13:40:32'),
(6, 13, '2021-09-30 13:40:33'),
(6, 13, '2021-09-30 13:40:34'),
(6, 13, '2021-09-30 13:40:35'),
(6, 13, '2021-09-30 13:40:36'),
(6, 13, '2021-09-30 13:40:37'),
(6, 13, '2021-09-30 13:40:38'),
(6, 13, '2021-09-30 13:40:39'),
(6, 13, '2021-09-30 13:40:40'),
(6, 13, '2021-09-30 13:40:41'),
(6, 13, '2021-09-30 13:40:45'),
(6, 13, '2021-09-30 13:40:47'),
(6, 13, '2021-09-30 13:40:48'),
(6, 13, '2021-09-30 13:40:49'),
(6, 13, '2021-09-30 13:40:50'),
(6, 13, '2021-09-30 13:40:51'),
(6, 13, '2021-09-30 13:40:52'),
(6, 13, '2021-09-30 13:40:53'),
(6, 13, '2021-09-30 13:40:54'),
(6, 13, '2021-09-30 13:40:55'),
(6, 13, '2021-09-30 13:40:56'),
(6, 13, '2021-09-30 13:40:57'),
(6, 13, '2021-09-30 13:40:58'),
(6, 13, '2021-09-30 13:40:59'),
(6, 13, '2021-09-30 13:41:00'),
(8, 13, '2021-09-30 13:12:31'),
(8, 13, '2021-09-30 13:40:05'),
(11, 13, '2021-09-30 13:09:12'),
(15, 13, '2021-09-30 13:41:29'),
(17, 13, '2021-09-30 13:41:38'),
(20, 13, '2021-09-30 13:41:26'),
(20, 13, '2021-09-30 13:41:36');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`Category_id`);

--
-- Индексы таблицы `categories_news`
--
ALTER TABLE `categories_news`
  ADD PRIMARY KEY (`news_id`,`category_id`),
  ADD KEY `categories_news_FK` (`category_id`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `pics_news_FK` (`pic_id`),
  ADD KEY `news_groups_FK` (`group_id`);

--
-- Индексы таблицы `picturies`
--
ALTER TABLE `picturies`
  ADD PRIMARY KEY (`pic_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- Индексы таблицы `users_views`
--
ALTER TABLE `users_views`
  ADD PRIMARY KEY (`news_id`,`user_id`,`view_date`),
  ADD KEY `user_views_FK` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `Category_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `picturies`
--
ALTER TABLE `picturies`
  MODIFY `pic_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `categories_news`
--
ALTER TABLE `categories_news`
  ADD CONSTRAINT `categories_news_FK` FOREIGN KEY (`category_id`) REFERENCES `categories` (`Category_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `news_categories_FK` FOREIGN KEY (`news_id`) REFERENCES `news` (`news_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_groups_FK` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `users_views`
--
ALTER TABLE `users_views`
  ADD CONSTRAINT `news_views_FK` FOREIGN KEY (`news_id`) REFERENCES `news` (`news_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `user_views_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
