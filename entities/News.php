<?php

namespace entities;

//Класс сущности новости, используется для
class News
{
    public  $newsID;
    public  $newsName;
    public  $text;
    public  $categories;
    public  $group;
    public $picture;
    public $currentCategory;

    public function __construct(int $newsID, string $newsName, string $text, array $categories, array $group, array $picture)
    {
        $this->newsID = $newsID;
        $this->newsName = $newsName;
        $this->text = $text;
        $this->categories = $categories;
        $this->group = $group;
        $this->picture = $picture;
    }

    public function getPicture(): array
    {
        return $this->picture;
    }

    public function getNewsID(): int
    {
        return $this->newsID;
    }

    public function setCurrentCategory($currentCategory)
    {
        $this->currentCategory = $currentCategory;
    }

    public function getCategories(): array
    {
        return $this->categories;
    }

    public function getGroup(): array
    {
        return $this->group;
    }

    public function getNewsName(): string
    {
        return $this->newsName;
    }

    public function getText(): string
    {
        return $this->text;
    }
}

