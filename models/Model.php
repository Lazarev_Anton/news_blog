<?php

namespace models;

use entities\News as News;
use repositories\UserRepository;
use repositories\NewsRepository;

require_once('repositories/NewsRepository.php');
require_once('repositories/UserRepository.php');

//класс модели, работающей с новостями
class Model
{
    private $repo;
    private $UserRepo;
    private $currentID;
    private $currentGroupPath;
    public $currentCategory;
    private static $instance = null;

    private function __construct()
    {
        $this->repo = new NewsRepository("localhost:3306", "news_blog", "root","");
        $this->UserRepo = new UserRepository("localhost:3306", "news_blog", "root","");
    }

    static public function GetInstance(): ?Model
    {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    // Проверка куки. Куки состоит из логина и хэша пароля пользователя
    public function CheckCookie(string $login, string $passwordHash) : bool
    {
        $result = $this->UserRepo->LoginUser($login, $passwordHash);
        if($result == $passwordHash) return true;
        return false;
    }

    // Возвращает текущую категорию, если пользователь просматривает ../[category-name]/
    public function GetCurrentCategory(): array
    {
        return $this->currentCategory;
    }

    // Устанавливает текущую директорию исходя из ../[category-name]/
    public function SetCurrentCategoryByPath(string $currentCategoryPath)
    {
        $this->currentCategory = $this->repo->getCategoryByPath($currentCategoryPath);
    }

    // возвращает новость по её id, используется в /news/{id}
    // возвращает null, если не нашел такую страницу
    public function GetNewsByID(int $id) : ?News
    {
        return $this->repo->getNews($id);
    }

    //Возвращает новость по категории группы. Если значения не указаны, то берутся текущие
    public function GetNewsByCategoryName($categoryName = null, $groupName = null) : array
    {

        if($categoryName == null) $categoryName = $this->currentCategory['name'];
        if($groupName == null) $groupName = $this->GetCurrentGroup()['group_id'];

        $news = $this->GetNewsByGroupId($groupName);
        $categorized_news = array();
        for ($i = 0;$i < sizeof($news);$i++) {
            foreach ($news[$i]->getCategories() as $thisCategory) {
                if($categoryName == $thisCategory['name']) {
                    array_push($categorized_news, $news[$i]);
                }
            }
        }

        return $categorized_news;
    }

    //возвращает ID новости, на которой находится пользователь
    public function GetCurrentID()
    {
        return $this->currentID;
    }

    //устанавливает ID новости, на которой находится пользователь
    public function SetCurrentID($currentID): void
    {
        $this->currentID = $currentID;
    }

    //возвращает список всех групп новостей
    public function GetAllGroups(): array
    {
        return $this->repo->GetAllGroups();
    }

    //возвращает массив новостей по id группы
    public function GetNewsByGroupId(int $id) : array
    {
        return $this->repo->GetNewsByGroupID($id);
    }

    //устанавливает текущий путь группы, полученный из ../group-[group_name]/
    public function SetGroupPath(string $path)
    {
        $this->currentGroupPath = $path;
    }

    //Возвращает текущую группу новостей
    public function  GetCurrentGroup() : ?array
    {
        if($this->currentGroupPath == null) return null;
        return $this->repo->getNewsGroupByName($this->currentGroupPath);
    }

    //Возвращает массив всех новостей, сгруппированные в массивы по группам
    public function GetAllNews() : array
    {
        $groups = $this->GetAllGroups();
        $news = [];
        foreach ($groups as $group) {
            array_push($news, $this->repo->GetNewsByGroupID($group['group_id']));
        }

        return $news;
    }

    //возвращает ассоциативный массив имя группы => количество новостей
    public function CountNewsByGroups() : array
    {
        $groups = $this->GetAllGroups();
        $newsCount = array();

        foreach ($groups as $group) {
            $newsCount[$group['name']] = sizeof($this->GetNewsByGroupId($group['group_id']));
        }
        return $newsCount;
    }

    //возвращает массив всех категорий
    public function GetAllCategories() : array
    {
        return $this->repo->GetAllCategories();
    }

    // возвращает ассоциативные массивы имя_категории => количество_новостей
    // сгруппированные в ассоциативный массив имя_группы => массив_категорий
    public function CountNewsByCategoriesInGroup() : array
    {
        $groups = $this->GetAllGroups();
        $categories = $this->repo->GetAllCategories();
        $newsCount = array();

        foreach ($groups as $group) {
            $categoryCount = [];
            foreach ($categories as $category) {
                $categoryCount[$category['name']] =
                    sizeof($this->GetNewsByCategoryName($category['name'], $group['group_id']));
            }
            $newsCount[$group['name']] = $categoryCount;
        }
        return $newsCount;
    }

    public function GetViewsByNewsID(int $newsID) : int
    {
        return $this->repo->GetViewsById($newsID);
    }

    public function AddView(string $login, int $newsID)
    {
        $userID = $this->UserRepo->getUserID($login);
        $this->repo->AddViewByUser($userID, $newsID);
    }
}
