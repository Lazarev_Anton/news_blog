<?php

namespace models;

use repositories\UserRepository;

require_once("../repositories/UserRepository.php");

//Класс модели, работающей с данными пользователя
class UserModel
{
    private $repo;
    private $salt = "saltForPassword";
    private static $instance = null;

    private function __construct()
    {
        $this->repo = new UserRepository("localhost:3306", "news_blog", "root","");
    }

    static public function getInstance(): ?UserModel
    {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    //Вход пользователя
    //Возвращает хэш пароля, если данные верны, иначе ошибку в строке
    public function LoginUser(string $login, string $password) : string
    {
        $passwordHash = md5(trim($password.$this->salt));
        $err = $this->repo->LoginUser($login, $passwordHash);
        if($err == null) return $passwordHash;
        else return $err;
    }

    //возвращает id пользователя по его логину
    public function getUserID(string $login) : ?int
    {
        return $this->repo->getUserID($login);
    }

    //Регистрация пользователя
    //Возвращает хэш пароля, если данные верны, иначе ошибку в строке
    public function RegisterUser(string $login, string $password) : string
    {
        $passwordHash = md5(trim($password.$this->salt));
        $err = $this->repo->RegisterUser($login, $passwordHash);
        if($err == null) return $passwordHash;
        else return $err;
    }
}