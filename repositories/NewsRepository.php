<?php

namespace repositories;

use entities\News as News;
use PDO;

require_once('entities/News.php');

//Класс репозитория, взаимодействует с БД через PDO
class NewsRepository
{
    private $DBH;

    public function __construct(string $host, string $dbName, string $user, string $dbPassword)
    {
        $this->DBH = new PDO("mysql:host=$host;dbname=$dbName", $user, $dbPassword);
    }

    //возвращает новость в виде объекта News по id новости в БД, если такая есть
    public function getNews(int $id) : ?News
    {
        $STH = $this->DBH->prepare("SELECT * FROM `news` WHERE `news_id` = :news_id");
        $STH->bindParam(':news_id', $id);

        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_CLASS, "News");
        $news = $STH->fetch();
        if($news != false){
            $categories = $this->getNewsCategories($news['news_id']);
            $picture = $this->getNewsPicture($news['news_id']);
            $group = $this->getNewsGroup($news['news_id']);
            return new News($news['news_id'], $news['news_name'], $news['text'], $categories, $group, $picture);
        }

        return null;
    }

    //Возвращает категорию по параметру path_name из БД
    public function getCategoryByPath(string $path) : ?array
    {
        $STH = $this->DBH->prepare("SELECT * FROM `categories` WHERE  `categories`.path_name = :path_name;");
        $STH->bindParam(":path_name",$path );
        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $data = $STH->fetch();
        if($data != null)
            $category = array(
                "category_id" => $data['category_id'],
                "name" => $data['name'],
                "description" => $data['description'],
                "path" => $data['path_name']
            );
        else return null;
        return $category;
    }

    //возвращает список категорий по id новости
    private function getNewsCategories(int $news_id) : array
    {
        $STH = $this->DBH->prepare("SELECT *  FROM categories_news, categories WHERE categories_news.news_id = :news_id and categories_news.category_id = categories.Category_id");
        $STH->bindParam(":news_id",$news_id );
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $categories = [];
        while ($data = $STH->fetch())
        {
            $category = array(
                "category_id" => $data['category_id'],
                "name" => $data['name'],
                "description" => $data['description'],
                "path" => $data['path_name']
            );

            array_push($categories, $category);
        }

        return $categories;
    }

    //возвращает путь и alt изображения новости по её id
    private function getNewsPicture(int $news_id) : array
    {
        $STH = $this->DBH->prepare("SELECT `alt`,`path` FROM `picturies`, news WHERE news.pic_id = picturies.pic_id and news.news_id = :news_id;");
        $STH->bindParam(":news_id",$news_id );
        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $data = $STH->fetch();

        $picture = array(
            "alt" => $data['alt'],
            "path" => $data['path']
        );

        return $picture;
    }

    //возвращает инофрмацию о группе новости по id новости.
    public function getNewsGroup(int $news_id) : array
    {
        $STH = $this->DBH->prepare("SELECT * FROM `groups`, `news` WHERE news.group_id = groups.group_id and news.news_id = :news_id;");
        $STH->bindParam(":news_id",$news_id );
        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $data = $STH->fetch();

        $group = array(
            "group_id" => $data['group_id'],
            "name" => $data['name'],
            "description" => $data['description'],
            "path" => $data['path_name']
        );

        return $group;
    }

    //Возвращает информацию о группе по параметру path_name в бд
    public function getNewsGroupByName(string $path) : ?array
    {
        $STH = $this->DBH->prepare("SELECT * FROM `groups` WHERE  `groups`.path_name = :path;");
        $STH->bindParam(":path",$path );
        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $data = $STH->fetch();
        if($data != null) {
            return array(
                "group_id" => $data['group_id'],
                "name" => $data['name'],
                "description" => $data['description'],
                "path" => $data['path_name']
            );
        }
        return null;
    }

    //возвращает инофрмацию обо всех группах, записанных в бд
    public function GetAllGroups(): array
    {
        $STH = $this->DBH->prepare("SELECT * FROM `groups`;");
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);

        $groups = array();

        while ($data = $STH->fetch()) {
            $group = array(
                "group_id" => $data['group_id'],
                "name" => $data['name'],
                "description" => $data['description'],
                "path" => $data['path_name']
            );
            array_push($groups, $group);
        }

        return $groups;
    }

    //возвращает информацию обо всех категориях, записанных в бд
    public function GetAllCategories() : array
    {
        $STH = $this->DBH->prepare("SELECT * FROM `categories`;");
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);

        $categories = array();

        while ($data = $STH->fetch()) {
            $category = array(
                "Category_id" => $data['Category_id'],
                "name" => $data['name'],
                "description" => $data['description'],
                "path" => $data['path_name']
            );
            array_push($categories, $category);
        }

        return $categories;
    }

    //Возвращает количество просмотров новости с id newsID
    public function GetViewsById(int $newsID) : int
    {
        $STH = $this->DBH->prepare("SELECT COUNT(*) as `views` FROM `users_views` WHERE `news_id`=:news_id;");
        $STH->bindParam(":news_id",$newsID);
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);

        $data = $STH->fetch();
        return $data['views'];
    }

    public function AddViewByUser(int $userID, int $newsID)
    {
        $STH = $this->DBH->prepare("INSERT INTO `users_views`(`news_id`, `user_id`) VALUES (:news_id, :user_id);");
        $STH->bindParam(":news_id",$newsID);
        $STH->bindParam(":user_id",$userID);
        $STH->execute();
    }

    //возвращает все новости с указанным параметром group_id в бд
    public function GetNewsByGroupID(int $id) : array
    {
        $STH = $this->DBH->prepare("SELECT * FROM `news` WHERE `group_id` = :group_id");
        $STH->bindParam(':group_id', $id);

        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_CLASS, "News");
        $GrouppedNews = [];
        while ($news = $STH->fetch()) {
            $categories = $this->getNewsCategories($news['news_id']);
            $picture = $this->getNewsPicture($news['news_id']);
            $group = $this->getNewsGroup($news['news_id']);
            array_push($GrouppedNews, new News($news['news_id'], $news['news_name'], $news['text'], $categories, $group, $picture));
        }

        return $GrouppedNews;
    }
}