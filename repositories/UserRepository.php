<?php

namespace repositories;

use PDO;

//Взаимодействует с данными пользователя в БД
class UserRepository
{

    private $DBH;

    public function __construct(string $host, string $dbName, string $user, string $dbPassword)
    {
        $this->DBH = new PDO("mysql:host=$host;dbname=$dbName", $user, $dbPassword);
    }

    //Проверяет наличие такой комбинации логина и пароля в БД
    public function LoginUser(string $login, string $passwordHash) : ?string
    {
        $STH = $this->DBH->prepare("SELECT * FROM `users` where `users`.`login` = :login;");
        $STH->bindParam(":login",$login );
        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $data = $STH->fetch();
        if($data != null) {
            if($data['password'] != $passwordHash) {
                return "invalid password";
            }
            return $passwordHash;
        }
        return "user not found";
    }

    //проверяет отсутствие такого логина в бд и заносит новую комбинацию логин/пароль
    public function RegisterUser(string $login, string $passwordHash) : ?string
    {
        $STH = $this->DBH->prepare("SELECT * FROM `users` where `users`.`login` = :login;");
        $STH->bindParam(":login",$login );
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $data = $STH->fetch();
        if($data == null) {
            $STH = $this->DBH->prepare("INSERT INTO `users`(`login`, `password`) VALUES (:login,:passHash)");
            $STH->bindParam(":login",$login );
            $STH->bindParam(":passHash",$passwordHash );
            $STH->execute();

            return null;
        }
        return "login already taken";
    }

    //возвращает id пользователя по его логину
    public function getUserID(string $login) : ?int
    {
        $STH = $this->DBH->prepare("SELECT * FROM `users` where `users`.`login` = :login;");
        $STH->bindParam(":login",$login );
        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $data = $STH->fetch();
        if($data != null) {
            return $data['user_id'];
        }
        return null;
    }
}
