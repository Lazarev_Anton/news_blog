<?php require_once("views/templates/header.php"); ?>
<section class="ptb-0">
    <div class="mb-30 brdr-ash-1 opacty-5"></div>
    <div class="container">
        <a class="mt-10" href="/news/"><i class="mr-5 ion-ios-home"></i>Новости<i class="mlr-10 ion-chevron-right"></i></a>
        <a class="color-ash mt-10">тупик</a>
    </div><!-- container -->
</section>


<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-12 col-md-8">
                <h3><b>404</b></h3>
                <p class="mb-30 mr-100 mr-sm-0">Мы не нашли страницу, которую вы искали</p>
            </div><!-- col-md-6 -->

            <div class="col-sm-12 col-md-4">
                <h3 class="mb-20 mt-sm-50"><b>INFORMATION</b></h3>

                <ul class="font-11 list-relative list-li-pl-30 list-block list-li-mb-15">
                    <li><i class="abs-tl ion-ios-location"></i>599 Co Rd 771 Ohio City CO <br/>81237. United States</li>
                    <li><i class="abs-tl ion-android-mail"></i>Infor.deercreative@gmail.com</li>
                    <li><i class="abs-tl ion-android-call"></i>(+12)-345-678-910</li>
                </ul>
                <ul class="font-11  list-a-plr-10 list-a-plr-sm-5 list-a-ptb-15 list-a-ptb-sm-5">
                    <li><a class="pl-0" href="#"><i class="ion-social-linkedin"></i></a></li>
                    <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                    <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                    <li><a href="#"><i class="ion-social-google"></i></a></li>
                    <li><a href="#"><i class="ion-social-pinterest"></i></a></li>
                </ul>

            </div><!-- col-md-6 -->
        </div><!-- row -->
    </div><!-- container -->
</section>

<?php require_once("views/templates/footer.php");?>
