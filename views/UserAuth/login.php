<?php require "views/templates/header.php";?>
<div class="container">
    <div class="col-sm-12 col-md-8">
        <h3><b>Войти</b></h3>
        <p class="mb-30 mr-100 mr-sm-0">Чтобы просмсотреть весь контент сайта, авторизируйтесь</p>
        <form action="../../controllers/LoginController.php" method="POST" class="form-block mb-2 form-bold form-mb-20 form-h-35 form-brdr-b-grey pr-50 pr-sm-0">
            <div class="row">

                <div class="col-sm-6">
                    <p class="color-ash">Login</p>
                    <div class="pos-relative">
                        <input name="login" class="pr-20" type="text">
                    </div><!-- pos-relative -->
                </div><!-- col-sm-6 -->


                <div class="col-sm-6">
                    <p class="color-ash">Password</p>
                    <div class="pos-relative">
                        <input name="password" class="pr-20" type="Password">
                    </div><!-- pos-relative -->



                </div><!-- col-sm-6 -->

                <button class=" font-20 plr-15 btn-fill-primary" type="submit">Войти</button>
                <div class="abs-br font-13 plr-15">Нет аккаунта? Вы можете <a href="/register/">зарегистрироваться</a></div>
            </div><!-- row -->
        </form>
    </div><!-- col-md-6 -->
</div>
<?php require "views/templates/footer.php";?>