<?php
//category endpoint

use models\Model;

require_once('models/Model.php');

$model = Model::GetInstance();
$thisGroup = $model->GetCurrentGroup();
$thisCategory = $model->GetCurrentCategory();
$thisNews = $model->GetNewsByCategoryName();
require "views/templates/header.php";

?>
<section>
    <div class="container">
        <a class="mt-10" href="../../news/"><i class="mr-5 ion-ios-home"></i>Новости<i class="mlr-10 ion-chevron-right"></i></a>
        <a class="mt-10" ><?php echo $thisGroup['name'];?></a>
    </div><!-- container -->

    <div class="container">
        <div class="row">

            <div class="col-md-12 col-lg-8">



                <h4 class="p-title mt-30"><b><?php echo $thisCategory['name']." в группе ".$thisGroup['name'];?></b></h4>
                <div class="row">

                    <?php
                    for($i = 0;$i < sizeof($thisNews);$i++) {
                        ?>

                        <div class="col-sm-6">
                            <img src="/../views/images/<?php $pic =$thisNews[$i]->getPicture(); echo $pic['path'];?>" alt="<?php echo $pic['alt'];?>">
                            <h4 class="pt-20"><a href="/news/<?php echo $thisNews[$i]->getNewsID();?>"><b><?php echo $thisNews[$i]->getNewsName();?></b></a></h4>
                            <ul class="list-li-mr-20 pt-10 mb-30">
                                <li class="color-lite-black">by <a href="#" class="color-black"><b>Olivia Capzallo,</b></a>
                                    Jan 25, 2018</li>
                                <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo $model->GetViewsByNewsID($thisNews[$i]->getNewsID());?></li>
                            </ul>
                        </div><!-- col-sm-6 -->

                        <?php
                    }
                    ?>



                </div><!-- row -->


            </div><!-- col-md-9 -->

            <div class="d-none d-md-block d-lg-none col-md-3"></div>
            <div class="col-md-6 col-lg-4">
                <div class="pl-20 pl-md-0">

                    <?php require "views/templates/Popular.php" ?>

                    <div class="mtb-50 mb-md-0">
                        <h4 class="p-title"><b>NEWSLETTER</b></h4>
                        <p class="mb-20">Subscribe to our newsletter to get notification about new updates,
                            information, discount, etc..</p>
                        <form class="nwsltr-primary-1">
                            <input type="text" placeholder="Your email"/>
                            <button type="submit"><i class="ion-ios-paperplane"></i></button>
                        </form>
                    </div><!-- mtb-50 -->

                </div><!--  pl-20 -->
            </div><!-- col-md-3 -->

        </div><!-- row -->
    </div><!-- container -->
</section>

<?php require "views/templates/footer.php";?>
