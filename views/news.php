<?php
//News endpoint

use models\Model;

require_once('models/Model.php');

$model = Model::GetInstance();
$thisNews = $model->GetAllNews();
$groups = $model->GetAllGroups();
if($model->GetCurrentGroup()['group_id'] != null) $filteredNews = array( $model->GetNewsByGroupId($model->GetCurrentGroup()['group_id']));
else $filteredNews = $thisNews;
require "views/templates/header.php";

?>



<div class="container">
    <div class="h-600x h-sm-auto">
        <div class="h-2-3 h-sm-auto oflow-hidden">

            <!--first news-->
            <div class="pb-5 pr-5 pr-sm-0 float-left float-sm-none w-2-3 w-sm-100 h-100 h-sm-300x">
                <a class="pos-relative h-100 dplay-block" href="/news/<?php echo $thisNews[0][0]->getNewsID();?>">

                    <div class="img-bg bg-grad-layer-6" style="background: url('/../views/images/<?php $pic =$thisNews[0][0]->getPicture(); echo $pic['path'];?>') no-repeat center; background-size: cover;"></div>

                    <div class="abs-blr color-white p-20 bg-sm-color-7">
                        <h3 class="mb-15 mb-sm-5 font-sm-13"><b><?php echo $thisNews[0][0]->getNewsName();?></b></h3>
                        <ul class="list-li-mr-20">
                            <li>by <span class="color-primary"><b>Olivia Capzallo</b></span> Jan 25, 2018</li>
                            <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo $model->GetViewsByNewsID($thisNews[0][0]->getNewsID());?></li>
                        </ul>
                    </div><!--abs-blr -->
                </a><!-- pos-relative -->
            </div><!-- w-1-3 -->

            <!--2 and 3 news-->
            <div class="float-left float-sm-none w-1-3 w-sm-100 h-100 h-sm-600x">

                <div class="pl-5 pb-5 pl-sm-0 ptb-sm-5 pos-relative h-50">
                    <a class="pos-relative h-100 dplay-block" href="/news/<?php echo $thisNews[0][1]->getNewsID();?>">

                        <div class="img-bg bg-grad-layer-6" style="background: url('/../views/images/<?php $pic =$thisNews[0][1]->getPicture(); echo $pic['path'];?>') no-repeat center; background-size: cover;"></div>

                        <div class="abs-blr color-white p-20 bg-sm-color-7">
                            <h4 class="mb-10 mb-sm-5"><b><?php echo $thisNews[0][1]->getNewsName();?></b></h4>
                            <ul class="list-li-mr-20">
                                <li>Jan 25, 2018</li>
                                <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo $model->GetViewsByNewsID($thisNews[0][1]->getNewsID());?></li>
                            </ul>
                        </div><!--abs-blr -->
                    </a><!-- pos-relative -->
                </div><!-- w-1-3 -->

                <div class="pl-5 ptb-5 pl-sm-0 pos-relative h-50">
                    <a class="pos-relative h-100 dplay-block" href="/news/<?php echo $thisNews[0][2]->getNewsID();?>">

                        <div class="img-bg bg-grad-layer-6" style="background: url('/../views/images/<?php $pic =$thisNews[0][2]->getPicture(); echo $pic['path'];?>') no-repeat center; background-size: cover;"></div>

                        <div class="abs-blr color-white p-20 bg-sm-color-7">
                            <h4 class="mb-10 mb-sm-5"><b><?php echo $thisNews[0][2]->getNewsName();?></b></h4>
                            <ul class="list-li-mr-20">
                                <li>Jan 25, 2018</li>
                                <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo $model->GetViewsByNewsID($thisNews[0][2]->getNewsID());?></li>
                            </ul>
                        </div><!--abs-blr -->
                    </a><!-- pos-relative -->
                </div><!-- w-1-3 -->

            </div><!-- float-left -->

        </div><!-- h-2-3 -->

        <!-- 4-6 news -->
        <div class="h-1-3 oflow-hidden">

            <div class="pr-5 pr-sm-0 pt-5 float-left float-sm-none pos-relative w-1-3 w-sm-100 h-100 h-sm-300x">
                <a class="pos-relative h-100 dplay-block" href="/news/<?php echo $thisNews[0][3]->getNewsID();?>">

                    <div class="img-bg  bg-grad-layer-6" style="background: url('/../views/images/<?php $pic =$thisNews[0][3]->getPicture(); echo $pic['path'];?>') no-repeat center; background-size: cover;"></div>

                    <div class="abs-blr color-white p-20 bg-sm-color-7">
                        <h4 class="mb-10 mb-sm-5"><b><?php echo $thisNews[0][3]->getNewsName();?></b></h4>
                        <ul class="list-li-mr-20">
                            <li>Jan 25, 2018</li>
                            <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo $model->GetViewsByNewsID($thisNews[0][3]->getNewsID());?></li>
                        </ul>
                    </div><!--abs-blr -->
                </a><!-- pos-relative -->
            </div><!-- w-1-3 -->

            <div class="plr-5 plr-sm-0 pt-5 pt-sm-10 float-left float-sm-none pos-relative w-1-3 w-sm-100 h-100 h-sm-300x">
                <a class="pos-relative h-100 dplay-block" href="/news/<?php echo $thisNews[0][4]->getNewsID();?>">

                    <div class="img-bg bg-grad-layer-6" style="background: url('/../views/images/<?php $pic =$thisNews[0][4]->getPicture(); echo $pic['path'];?>') no-repeat center; background-size: cover;"></div>

                    <div class="abs-blr color-white p-20 bg-sm-color-7">
                        <h4 class="mb-10 mb-sm-5"><b><?php echo $thisNews[0][4]->getNewsName();?></b></h4>
                        <ul class="list-li-mr-20">
                            <li>Jan 25, 2018</li>
                            <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo $model->GetViewsByNewsID($thisNews[0][4]->getNewsID());?></li>
                        </ul>
                    </div><!--abs-blr -->
                </a><!-- pos-relative -->
            </div><!-- w-1-3 -->

            <div class="pl-5 pl-sm-0 pt-5 pt-sm-10 float-left float-sm-none pos-relative w-1-3 w-sm-100 h-100 h-sm-300x">
                <a class="pos-relative h-100 dplay-block" href="/news/<?php echo $thisNews[0][5]->getNewsID();?>">

                    <div class="img-bg bg-grad-layer-6" style="background: url('/../views/images/<?php $pic = $thisNews[0][5]->getPicture(); echo $pic['path'];?>') no-repeat center; background-size: cover;"></div>

                    <div class="abs-blr color-white p-20 bg-sm-color-7">
                        <h4 class="mb-10 mb-sm-5"><b><?php echo $thisNews[0][5]->getNewsName();?></b></h4>
                        <ul class="list-li-mr-20">
                            <li>Jan 25, 2018</li>
                            <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo $model->GetViewsByNewsID($thisNews[0][5]->getNewsID());?></li>
                        </ul>
                    </div><!--abs-blr -->
                </a><!-- pos-relative -->
            </div><!-- w-1-3 -->

        </div><!-- h-2-3 -->
    </div><!-- h-100vh -->
</div><!-- container -->


<section>
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-lg-8">


                <h4 class="p-title mt-30">
                    <form class="form-inline" action="news/filter" method="get">
                        <b class="mr-3"><?php if($model->GetCurrentGroup()['group_id'] != null)
                                                echo $model->GetCurrentGroup()['name'];
                            else echo "all";?> news</b>
                        <select class="mr-3" name="group" id="filter">
                            <option value="all">все</option>
                            <?php foreach ($groups as $group){ ?>
                            <option value="<?php echo $group['path']?>"><?php echo $group['name']?></option>
                            <?php }?>
                        </select>
                        <button class="btn-fill-primary p-1" type="submit">применить фильтр</button>
                    </form>
                </h4>
                <div class="row">
                    <?php
                        for($i = 0;$i < sizeof($filteredNews);$i++){
                            for ($j = 0; $j < sizeof($filteredNews[$i]); $j++) {
                                if($i > 0 || $j > 5 || $model->GetCurrentGroup()['group_id'] != null) {
                    ?>
                    <div class="col-sm-6">
                        <img src="/../views/images/<?php $pic =$filteredNews[$i][$j]->getPicture(); echo $pic['path'];?>" alt="<?php echo $pic['alt'];?>">
                        <h4 class="pt-20"><a href="/news/<?php echo $filteredNews[$i][$j]->getNewsID();?>"><b><?php echo $filteredNews[$i][$j]->getNewsName();?></b></a></h4>
                        <ul class="list-li-mr-20 pt-10 mb-30">
                            <li class="color-lite-black">by <a href="#" class="color-black"><b>Olivia Capzallo,</b></a>
                                Jan 25, 2018</li>
                            <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo $model->GetViewsByNewsID($filteredNews[$i][$j]->getNewsID());?></li>
                        </ul>
                    </div><!-- col-sm-6 -->
                    <?php
                                }
                            }
                        }
                    ?>
                </div><!-- row -->
            </div><!-- col-md-9 -->

            <div class="d-none d-md-block d-lg-none col-md-3"></div>
            <div class="col-md-6 col-lg-4">
                <div class="pl-20 pl-md-0">
                    <?php require "views/templates/Popular.php" ?>
                    <div class="mtb-50 mb-md-0">
                        <h4 class="p-title"><b>NEWSLETTER</b></h4>
                        <p class="mb-20">Subscribe to our newsletter to get notification about new updates,
                            information, discount, etc..</p>
                        <form class="nwsltr-primary-1">
                            <input type="text" placeholder="Your email"/>
                            <button type="submit"><i class="ion-ios-paperplane"></i></button>
                        </form>
                    </div><!-- mtb-50 -->
                </div><!--  pl-20 -->
            </div><!-- col-md-3 -->
        </div><!-- row -->
    </div><!-- container -->
</section>
<?php require "views/templates/footer.php";?>
