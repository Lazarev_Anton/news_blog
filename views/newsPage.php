<?php

use models\Model;

require_once('models/Model.php');

$model = Model::GetInstance();
$thisNews = $model->GetNewsByID($model->GetCurrentID());
$model->AddView($_COOKIE['login'], $model->GetCurrentID());
?>

<?php require "views/templates/header.php" ?>

<section class="ptb-0">

    <div class="mb-30 brdr-ash-1 opacty-5"></div>
    <div class="container">
        <a class="mt-10" href="../../news/"><i class="mr-5 ion-ios-home"></i>Новости<i class="mlr-10 ion-chevron-right"></i></a>
        <a class="mt-10" href="../../news/group-<?php echo $thisNews->group['path'] ;?>"><?php echo $thisNews->group['name'] ;?><i class="mlr-10 ion-chevron-right"></i></a>
        <a class="mt-10 color-ash" ><?php echo $thisNews->getNewsName(); ?></a>
    </div><!-- container -->
</section>


<section>
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-lg-8">
                <!--NEWS PIC WILL BE HERE-->
                <img src="/../news_blog/views/images/<?php echo $thisNews->picture['path']; ?>" alt="<?php echo $thisNews->picture['alt']; ?>">
                <!--NEWS NAME WILL BE HERE-->
                <h3 class="mt-30"><b><?php echo $thisNews->getNewsName(); ?></b></h3>
                <ul class="list-li-mr-20 mtb-15">
                    <li>by <a href="#"><b>Olivia Capzallo </b></a> Jan 25, 2018</li>
                    <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo $model->GetViewsByNewsID($model->GetCurrentID());?></li>
                </ul>

                <!--NEWS TEXT WILL BE HERE-->
                <p><?php echo $thisNews->getText(); ?> </p>
                <!--/ NEWS TEXT WILL BE HERE-->
                <div class="float-left-right text-center mt-40 mt-sm-20">
                    <!--category tags will be here-->
                    <ul class="mb-30 list-li-mt-10 list-li-mr-5 list-a-plr-15 list-a-ptb-7 list-a-bg-grey list-a-br-2 list-a-hvr-primary ">
                        <?php
                            $thisGroup = $thisNews->group['path'] ;
                            foreach ($thisNews->categories as $category){
                                $thisName = $category['name'];
                                $thisPathName = $category['path'];

                                echo "<li><a href='../../news/group-$thisGroup/$thisPathName'>$thisName</a></li>";
                            }
                        ?>
                    </ul>
                    <ul class="mb-30 list-a-bg-grey list-a-hw-radial-35 list-a-hvr-primary list-li-ml-5">
                        <li class="mr-10 ml-0">Share</li>
                        <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                        <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                        <li><a href="#"><i class="ion-social-google"></i></a></li>
                        <li><a href="#"><i class="ion-social-instagram"></i></a></li>
                    </ul>

                </div><!-- float-left-right -->

                <div class="brdr-ash-1 opacty-5"></div>

                <h4 class="p-title mt-50"><b>YOU MAY ALSO LIKE</b></h4>
                <div class="row">
                    <!--TODO: make it with news from db-->
                    <div class="col-sm-6">
                        <img src="../../news_blog/views/images/crypto-news-2-600x450.jpg" alt="">
                        <h4 class="pt-20"><a href="#"><b>2017 Market Performance: <br/>Crypto vs.Stock</b></a></h4>
                        <ul class="list-li-mr-20 pt-10 mb-30">
                            <li class="color-lite-black">by <a href="#" class="color-black"><b>Olivia Capzallo,</b></a>
                                Jan 25, 2018</li>
                            <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i>30,190</li>
                            <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i>47</li>
                        </ul>
                    </div><!-- col-sm-6 -->

                    <div class="col-sm-6">
                        <img src="../../news_blog/views/images/crypto-news-1-600x450.jpg" alt="">
                        <h4 class="pt-20"><a href="#"><b>2017 Market Performance: Crypto vs.Stock</b></a></h4>
                        <ul class="list-li-mr-20 pt-10 mb-30">
                            <li class="color-lite-black">by <a href="#" class="color-black"><b>Olivia Capzallo,</b></a>
                                Jan 25, 2018</li>
                            <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i>30,190</li>
                            <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i>47</li>
                        </ul>
                    </div><!-- col-sm-6 -->

                </div><!-- row -->


            </div><!-- col-md-9 -->

            <div class="d-none d-md-block d-lg-none col-md-3"></div>
            <div class="col-md-6 col-lg-4">
                <div class="pl-20 pl-md-0">
                    <?php require "views/templates/Popular.php" ?>

                    <div class="mtb-50 mb-md-0">
                        <h4 class="p-title"><b>NEWSLETTER</b></h4>
                        <p class="mb-20">Subscribe to our newsletter to get notification about new updates,
                            information, discount, etc..</p>
                        <form class="nwsltr-primary-1">
                            <input type="text" placeholder="Your email"/>
                            <button type="submit"><i class="ion-ios-paperplane"></i></button>
                        </form>
                    </div><!-- mtb-50 -->

                </div><!--  pl-20 -->
            </div><!-- col-md-3 -->

        </div><!-- row -->

    </div><!-- container -->
</section>


<?php require "views/templates/footer.php";?>

