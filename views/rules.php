<?php

use models\Model;

// /rules/ endpoint
require_once("views/templates/header.php");
?>
<section class="ptb-0">
    <div class="mb-30 brdr-ash-1 opacty-5"></div>
    <div class="container">
        <a class="mt-10" href="/news/"><i class="mr-5 ion-ios-home"></i>Новости<i class="mlr-10 ion-chevron-right"></i></a>
        <a class="color-ash mt-10">Правила</a>
    </div><!-- container -->
</section>


<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-12 col-md-8">
                <h3><b>Правила</b></h3>
                <p class="mb-30 mr-100 mr-sm-0">Pellentesque pharetra nec dolor sed tristique. Donec elementum dictum ligula nec hendrerit. Aenean pulvinar ante non molestie porttitor. Fusce porta tincidunt est, eu auctor libero venenatis vitae. Suspendisse gravida dolor semper dictum lobortis. Etiam congue lacinia leo, et condimentum ante congue et. Aenean id faucibus quam.</p>
                <p class="mb-30 mr-100 mr-sm-0">Duis bibendum sed eros vitae molestie. Phasellus porttitor lobortis lacus, a auctor magna sollicitudin vitae. Sed vulputate turpis vitae malesuada semper. Integer condimentum diam ac nisl ullamcorper, vel ultricies mauris lacinia. Curabitur at scelerisque neque. Proin et nibh nec orci bibendum convallis. Aenean interdum ante quis semper dignissim. Phasellus volutpat ante nec tincidunt aliquet. Mauris aliquam facilisis ornare.</p>
                <p class="mb-30 mr-100 mr-sm-0">Morbi cursus convallis est, nec mollis dolor porta sed. Mauris ut lorem sit amet lacus tincidunt mollis ac quis magna. Nam porta in orci ut pharetra. Donec faucibus tortor eu tellus malesuada dapibus. Suspendisse ac risus ut ipsum pulvinar blandit et eget ligula. Sed malesuada lobortis magna, nec lobortis eros aliquet et. Aenean in mauris felis. Sed ornare elit a feugiat rhoncus. Phasellus egestas turpis pretium ligula ultrices vestibulum. Suspendisse malesuada non sapien vitae faucibus. Praesent scelerisque ultrices maximus. Praesent id velit id libero hendrerit laoreet quis sit amet sapien. Sed consectetur, nunc nec venenatis ultrices, ante diam volutpat diam, tincidunt sollicitudin erat mi vitae ligula. Integer ultricies nulla non sagittis ullamcorper. Integer in quam pretium, dignissim nibh sed, molestie erat. Sed ullamcorper facilisis fringilla.</p>
                <p class="mb-30 mr-100 mr-sm-0">Maecenas hendrerit efficitur mauris et suscipit. Ut a erat ante. Vestibulum est turpis, semper vehicula viverra in, elementum eu leo. Duis eleifend erat vitae tellus molestie placerat. Vivamus interdum erat vel suscipit euismod. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi malesuada risus sit amet quam euismod, et convallis ante fringilla. Vestibulum gravida scelerisque mauris, nec dapibus augue vulputate id. Praesent vel dignissim massa.</p>
            </div><!-- col-md-6 -->

            <div class="col-sm-12 col-md-4">
                <h3 class="mb-20 mt-sm-50"><b>INFORMATION</b></h3>

                <ul class="font-11 list-relative list-li-pl-30 list-block list-li-mb-15">
                    <li><i class="abs-tl ion-ios-location"></i>599 Co Rd 771 Ohio City CO <br/>81237. United States</li>
                    <li><i class="abs-tl ion-android-mail"></i>Infor.deercreative@gmail.com</li>
                    <li><i class="abs-tl ion-android-call"></i>(+12)-345-678-910</li>
                </ul>
                <ul class="font-11  list-a-plr-10 list-a-plr-sm-5 list-a-ptb-15 list-a-ptb-sm-5">
                    <li><a class="pl-0" href="#"><i class="ion-social-linkedin"></i></a></li>
                    <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                    <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                    <li><a href="#"><i class="ion-social-google"></i></a></li>
                    <li><a href="#"><i class="ion-social-pinterest"></i></a></li>
                </ul>

            </div><!-- col-md-6 -->
        </div><!-- row -->
    </div><!-- container -->
</section>

<?php require_once("views/templates/footer.php");?>
