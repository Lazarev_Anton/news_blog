<?php

use models\Model;

require_once('models/Model.php');

$model = Model::GetInstance();

?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <title>NEWS</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans+Expanded:400,600,700" rel="stylesheet">

    <!-- Stylesheets -->
    <link href="../../news_blog/views/plugin-frameworks/bootstrap.css" rel="stylesheet">
    <link href="../../news_blog/views/fonts/ionicons.css" rel="stylesheet">
    <link href="../../news_blog/views/common/styles.css" rel="stylesheet">
</head>
<body>
<header>


    <div class="container">
        <a class="logo" href="/news/"><img src="../../news_blog/views/images/logo-black.png" alt="Logo"></a>




        <a class="menu-nav-icon" data-menu="#main-menu" href="#"><i class="ion-navicon"></i></a>

        <ul class="main-menu" id="main-menu">
            <li><a href="/news/">NEWS</a></li>
            <li class="drop-down"><a href="#">Groups<i class="ion-arrow-down-b"></i></a>
                <ul class="drop-down-menu drop-down-inner">
                    <?php
                    $groups = $model->GetAllGroups();
                    foreach ($groups as $group){
                        $name = $group['name'];
                        $path = $group['path'];
                        echo "<li><a href='../../news/group-$path'>$name</a></li>";
                    };
                    ?>
                </ul>
            </li>
            <li><a href="/about/">О НАС</a></li>
            <li><a href="/rules/">ПРАВИЛА</a></li>
            <?php if(!isset($_COOKIE['login']) || !isset($_COOKIE['hash'])){ ?>
            <li class="drop-down"><a href="#">Вход<i class="ion-arrow-down-b"></i></a>
                <ul class="drop-down-menu drop-down-inner">
                    <li><a href="/login/">Войти</a></li>
                    <li><a href="/register/">Регистрация</a></li>
                </ul>
            </li>
            <?php } else {
                echo "<li><a href='/logout/'>Выйти</a></li>";
            } ?>

        </ul>
        <div class="clearfix"></div>
    </div><!-- container -->

</header>

